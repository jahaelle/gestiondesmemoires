package com.Gemoire.Gemoire;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GemoireApplication {

	public static void main(String[] args) {
		SpringApplication.run(GemoireApplication.class, args);
	}

}
